<?php

date_default_timezone_set('Asia/Jakarta');

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Device');
        $this->load->model('M_History');
    }

    public function index()
    {
        $this->load->view('welcome');
    }

    public function login()
    {
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);

        $user = $this->db->get_where('user', ['username' => $username])->row();
        if ($user) {
            if (password_verify($password, $user->password)) {
                echo json_encode([
                    'status' => 'success',
                    'login' => true,
                    'message' => 'Anda berhasil login'
                ]);
            } else {
                echo json_encode([
                    'status' => 'failed',
                    'login' => false,
                    'message' => 'Username atau password salah'
                ]);
            }
        } else {
            echo json_encode([
                'status' => 'failed',
                'login' => false,
                'message' => 'Username atau password salah'
            ]);
        }
    }

    public function get_device()
    {
        $device = $this->M_Device->get();
        $now = strtotime(date('Y-m-d H:i'));

        foreach ($device as $dev) {
            $time = date('Y-m-d H:i', strtotime('+4 minutes', strtotime($dev->time)));
            $newtime = strtotime($time);

            $this->M_Device->update($dev->id, [
                'daya_before' => floatval($dev->tegangan_before) * floatval($dev->arus_before) . 'mWatt',
                'daya_after' => floatval($dev->tegangan_after) * floatval($dev->arus_after) . 'mWatt',
            ], ['id' => $dev->id]);

            if ($now > $newtime) {
                $this->M_Device->update($dev->id, [
                    'status' => 'Offline',
                ], ['id' => $dev->id]);
            }
        }

        echo json_encode([
            'device' => $device
        ]);
    }

    public function store_device()
    {
        $this->form_validation->set_rules("tegangan_before", "Tegangan Before", "required");
        $this->form_validation->set_rules("tegangan_after", "Tegangan After", "required");
        $this->form_validation->set_rules("arus_before", "Arus Before", "required");
        $this->form_validation->set_rules("arus_after", "Arus After", "required");
        // $this->form_validation->set_rules("daya_before", "Daya Before", "required");
        // $this->form_validation->set_rules("daya_after", "Daya After", "required");
        $this->form_validation->set_rules("dimmer", "Dimmer", "required");
        $this->form_validation->set_rules("ldr", "LDR", "required");
        $this->form_validation->set_rules("status", "Status", "required");

        if ($this->form_validation->run() == false) {
            echo json_encode([
                'status' => 'failed',
                'message' => 'Data tidak valid, periksa kembali data yang anda kirimkan'
            ]);
        } else {
            $data = [
                'tegangan_before' => $this->input->post('tegangan_before', true),
                'tegangan_after' => $this->input->post('tegangan_after', true),
                'arus_before' => $this->input->post('arus_before', true),
                'arus_after' => $this->input->post('arus_after', true),
                // 'daya_before' => floatval($this->input->post('tegangan_before', true)) * floatval($this->input->post('arus_before', true)) . 'mWatt',
                // 'daya_after' => floatval($this->input->post('tegangan_after', true)) * floatval($this->input->post('arus_after', true)) . 'mWatt',
                'dimmer' => $this->input->post('dimmer', true),
                'ldr' => $this->input->post('ldr', true),
                'status' => $this->input->post('status', true),
            ];

            $device = $this->M_Device->create($data);

            if ($device) {
                echo json_encode([
                    'status' => 'success',
                    'message' => 'Device berhasil ditambahkan',
                ]);
            } else {
                echo json_encode([
                    'status' => 'failed',
                    'message' => 'Device gagal ditambahkan'
                ]);
            }
        }
    }

    public function show_device($id)
    {
        $device = $this->M_Device->find($id);
        $now = strtotime(date('Y-m-d H:i'));
        $time = date('Y-m-d H:i', strtotime('+4 minutes', strtotime($device->time)));
        $newtime = strtotime($time);

        if ($now > $newtime) {
            $this->M_Device->update($id, ['status' => 'Offline']);
        }

        if ($device) {
            echo json_encode([
                'status' => 'success',
                'device' => $device,
            ]);
        } else {
            echo json_encode([
                'status' => 'failed',
                'message' => 'Device tidak ditemukan'
            ]);
        }
    }

    public function delete_device($id)
    {
        $device = $this->M_Device->delete($id);

        if ($device) {
            echo json_encode([
                'status' => 'success',
                'device' => 'Device berhasil dihapus'
            ]);
        } else {
            echo json_encode([
                'status' => 'failed',
                'message' => 'Device gagal dihapus'
            ]);
        }
    }

    public function send_data_device()
    {
        $this->form_validation->set_rules("id", "Id Device", "required");
        $this->form_validation->set_rules("tegangan_before", "Tegangan Before", "required");
        $this->form_validation->set_rules("tegangan_after", "Tegangan After", "required");
        $this->form_validation->set_rules("arus_before", "Arus Before", "required");
        $this->form_validation->set_rules("arus_after", "Arus After", "required");
        $this->form_validation->set_rules("dimmer", "Dimmer", "required");
        $this->form_validation->set_rules("ldr", "LDR", "required");
        $this->form_validation->set_rules("status", "Status", "required");

        if ($this->form_validation->run() == false) {
            echo json_encode([
                'status' => 'failed',
                'message' => 'Data tidak valid, periksa kembali data yang anda kirimkan'
            ]);
        } else {
            $id = $this->input->post('id', true);
            $device = $this->M_Device->find($id);

            if ($device) {
                $data = [
                    'tegangan_before' => $this->input->post('tegangan_before', true),
                    'tegangan_after' => $this->input->post('tegangan_after', true),
                    'arus_before' => $this->input->post('arus_before', true),
                    'arus_after' => $this->input->post('arus_after', true),
                    'daya_before' => floatval($this->input->post('tegangan_before', true)) * floatval($this->input->post('arus_before', true)) . 'mWatt',
                    'daya_after' => floatval($this->input->post('tegangan_after', true)) * floatval($this->input->post('arus_after', true)) . 'mWatt',
                    'dimmer' => $this->input->post('dimmer', true),
                    'ldr' => $this->input->post('ldr', true),
                    'status' => $this->input->post('status', true),
                ];

                $update = $this->M_Device->update($id, $data);

                $data['device_id'] = $id;
                $this->M_History->create($data);

                $this->M_Device->update($device->id, [
                    'time' => date('Y-m-d H:i:s'),
                    'status' => 'Online'
                ]);

                if ($update) {
                    echo json_encode([
                        'status' => 'success',
                        'message' => 'Data berhasil dikirim',
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'failed',
                        'message' => 'Data gagal dikirim'
                    ]);
                }
            } else {
                echo json_encode([
                    'status' => 'failed',
                    'message' => 'Device tidak ditemukan'
                ]);
            }
        }
    }

    public function get_history()
    {
        $history = $this->M_History->get();

        echo json_encode([
            'status' => 'success',
            'message' => 'Semua history',
            'history' => $history,
        ]);
    }

    public function get_history_device($id)
    {
        $history = $this->M_History->get_device($id);

        foreach ($history as $dev) {
            $this->M_History->update($dev->id, [
                'daya_before' => floatval($dev->tegangan_before) * floatval($dev->arus_before) . 'mWatt',
                'daya_after' => floatval($dev->tegangan_after) * floatval($dev->arus_after) . 'mWatt',
            ], ['id' => $dev->id]);
        }

        echo json_encode([
            'status' => 'success',
            'message' => 'Semua history device',
            'history' => $history,
        ]);
    }

    public function update_profile()
    {
        $this->form_validation->set_rules("id", "Id User", "required");
        $this->form_validation->set_rules("username", "Username", "required");
        $this->form_validation->set_rules("nama", "Nama", "required");
        $this->form_validation->set_rules("password", "Password", "required");

        if ($this->form_validation->run() == false) {
            echo json_encode([
                'status' => 'failed',
                'message' => 'Data tidak valid, periksa kembali data yang anda kirimkan'
            ]);
        } else {
            $id = $this->input->post('id', true);
            $user = $this->db->get_where('user', ['id' => $id])->row();

            if ($user) {
                $data = [
                    'username' => $this->input->post('username', true),
                    'nama' => $this->input->post('nama', true),
                    'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                ];

                $this->db->where('id', $id);
                $update = $this->db->update('user', $data);

                if ($update) {
                    echo json_encode([
                        'status' => 'success',
                        'message' => 'Profile berhasil diupdate',
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'failed',
                        'message' => 'Profile gagal diupdate'
                    ]);
                }
            } else {
                echo json_encode([
                    'status' => 'failed',
                    'message' => 'User tidak ditemukan'
                ]);
            }
        }
    }

    public function check_online($id)
    {
        $device = $this->M_Device->find($id);

        if ($device) {
            $data = ['status' => 'Offline'];

            $this->db->where('id', $id);
            $update = $this->db->update('device', $data);

            if ($update) {
                echo json_encode([
                    'status' => 'success',
                    'message' => 'Status device offline'
                ]);
            } else {
                echo json_encode([
                    'status' => 'failed',
                    'message' => 'Status device gagal diubah'
                ]);
            }
        } else {
            echo json_encode([
                'status' => 'failed',
                'message' => 'Device tidak ditemukan'
            ]);
        }
    }
}
