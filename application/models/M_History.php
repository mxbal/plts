<?php

class M_History extends CI_Model
{
    public function get()
    {
        $this->db->order_by('waktu', 'DESC');
        return $this->db->get('history')->result();
    }

    public function create($data)
    {
        $this->db->insert('history', $data);
        return true;
    }

    public function find($id)
    {
        return $this->db->get_where('history', ['id' => $id])->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('history', $data);
        return true;
    }

    public function get_device($id)
    {
        $this->db->order_by('waktu', 'DESC');
        return $this->db->get_where('history', ['device_id' => $id])->result();
    }
}
