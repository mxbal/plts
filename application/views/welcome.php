<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Api Plts</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500&display=swap" rel="stylesheet">

    <style>
        * {
            font-family: 'Poppins', sans-serif;
        }
    </style>
</head>

<body>
    <div class="container my-5">
        <h1 class="text-center">Welcome To API PLTS</h1>

        <div class="row mt-3">
            <div class="col-md-12">
                <ul class="list-group">
                    <li class="list-group-item active">List api</li>
                    <li class="list-group-item">
                        <b>Get All Device (GET)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>get_device</span> <br>
                            Result : <br>
                            <span class="text-success">
                                {
                                "device": [
                                {
                                "id": "1",
                                "tegangan_before": "1",
                                "tegangan_after": "2",
                                "arus_before": "1",
                                "arus_after": "2",
                                "dimmer": "dim",
                                "ldr": "yes",
                                "status": "Online"
                                }
                                ]
                                }
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Create Device (POST)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>store_device</span> <br>
                            Params : <br>
                            <b class="text-dark">tegangan_before</b> => value<br>
                            <b class="text-dark">tegangan_after</b> => value<br>
                            <b class="text-dark">arus_before</b> => value<br>
                            <b class="text-dark">arus_after</b> => value<br>
                            <b class="text-dark">dimmer</b> => value<br>
                            <b class="text-dark">ldr</b> => value<br>
                            <b class="text-dark">status</b> => value<br>
                            Result : <br>
                            <span class="text-success">
                                {
                                "status": "success",
                                "message": "Device berhasil ditambahkan",
                                }
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Get Detail Device (GET)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>show_device/{your_id_device}</span> <br>
                            Result : <br>
                            <span class="text-success">
                                {"status":"success","device":{"id":"3","tegangan_before":"3","tegangan_after":"2","arus_before":"8","arus_after":"2","dimmer":"dimmer","ldr":"yes","status":"Offline","time":"2022-06-27
                                12:15:59"}}
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Send Data Device (POST)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>send_data_device</span> <br>
                            Params : <br>
                            <b class="text-dark">id</b> => value<br>
                            <b class="text-dark">tegangan_before</b> => value<br>
                            <b class="text-dark">tegangan_after</b> => value<br>
                            <b class="text-dark">arus_before</b> => value<br>
                            <b class="text-dark">arus_after</b> => value<br>
                            <b class="text-dark">dimmer</b> => value<br>
                            <b class="text-dark">ldr</b> => value<br>
                            <b class="text-dark">status</b> => value<br>
                            Result : <br>
                            <span class="text-success">
                                {
                                "status": "success",
                                "message": "Data berhasil dikirim"
                                }
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Get All History (GET)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>get_history</span> <br>
                            Result : <br>
                            <span class="text-success">
                                {"status":"success","message":"Semua
                                History","history":[{"id":"1","device_id":"1","tegangan_before":"3","tegangan_after":"2","arus_before":"8","arus_after":"2","dimmer":"dimmer","ldr":"yes","status":"Offline","waktu":"2022-06-05
                                09:28:28"}]}
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Get History Device (GET)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>get_history_device/{your_id_device}</span> <br>
                            Result : <br>
                            <span class="text-success">
                                {"status":"success","message":"Semua
                                history","history":[{"id":"1","device_id":"1","tegangan_before":"3","tegangan_after":"2","arus_before":"8","arus_after":"2","dimmer":"dimmer","ldr":"yes","status":"Offline","waktu":"2022-06-05
                                09:28:28"}]}
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Delete Device (POST)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>delete_device/{your_id_device}</span> <br>
                            Result : <br>
                            <span class="text-success">
                                {"status":"success","device":"Device berhasil dihapus"}
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Update Profile (POST)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>update_profile/</span> <br>
                            Params : <br>
                            <b class="text-dark">id</b> => value<br>
                            <b class="text-dark">username</b> => value<br>
                            <b class="text-dark">nama</b> => value<br>
                            <b class="text-dark">password</b> => value<br>
                            Result : <br>
                            <span class="text-success">
                                {"status":"success","message":"Profile berhasil diupdate"}
                            </span>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <b>Check Status Device (GET)</b>
                        <p>
                            Url : <br>
                            <span class="text-primary"><?= base_url('api/') ?>check_online/{your_id_device}</span> <br>
                            Result : <br>
                            <span class="text-success">
                                {"status":"success","message":"Status device offline"}
                            </span>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>