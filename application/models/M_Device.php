<?php

class M_Device extends CI_Model
{
    public function get()
    {
        return $this->db->get('device')->result();
    }

    public function create($data)
    {
        $this->db->insert('device', $data);
        return true;
    }

    public function find($id)
    {
        return $this->db->get_where('device', ['id' => $id])->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('device', $data);
        return true;
    }

    public function delete($id)
    {
        $this->db->delete('device', ['id' => $id]);
        return true;
    }
}
